# -*- coding: utf-8 -*-
"""
Created on Thu Feb 27 16:03:36 2014

@authors: Benoit, Niels
"""
from bpsim import enum
import simpy
import random
from collections import namedtuple

"""
Gateway_direction specifies the number of incoming/outgoing sequence flows of
a particular gateway
    Diverging : multiple outgoing / no more than one incoming
    Converging : multiple incoming / no more than one outgoing
    Mixed : multiple outgoing / multiple incoming
    Unspecified : no constraints
"""
Gateway_direction = enum(diverging=1, converging=2, mixed=3, unspecified=4)


class Activity(object):
    """
    Object that represents generic BPMN activity

    Parameters
    ----------
    name : str
        Name of the activity.
    component_id : str
        Unique identification of model component in BPMN XML.

    Attributes
    ----------
    name : str
        Name of the activity.
    component_id : str
        Unique identification of model component in BPMN XML.
    duration : named tuple
        Named tuple containing information on the activity duration (e.g. the
        applicable probability distribution with associated parameters)
    resource : SimPy Resource object
        Object of the SimPy Resource class.

    """

    def __init__(self, name, duration, component_id):
        self.name = name
        self.duration = duration
        self.component_id = component_id
        self.duration = None
        self.resource = None

    def __eq__(self, other):
        return (isinstance(other, self.__class__) and
                self.name == other.name)

    def activity_generator_function(self, env, init_start_delay, entity):
        """
        Activity generator function

        This generator function models the execution of an activity. The
        progress of the process will be halted during the execution of the
        activity.

        Parameters
        ----------
        env : SimPy Environment object
            SimPy Environment object that manages the simulation time and is
            used for the scheduling and processing of events.
        init_start_delay : {0, model_instance.simulation_run_duration + 1}
            Length of delay that is taken into account after initialization.
            Value is passed by the ModelInstance object (model_instance)
            responsible for scheduling activities and other simulation model
            objects. A delay of 0 will be passed for the starting process
            activities, meaning that their execution can start immediately. For
            the other activities, 'init_start_delay' takes the value of the
            'simulation_run_duration' parameter of the ModelInstance object
            plus one. This means that the execution of the activity is
            scheduled after the end of the simulation run and hence will never
            be executed. However, when the relevant starting requirements for
            the activity are satisfied, the generator function will be resumed
            at an earlier moment in the simulation and the activity will be
            executed.
        entity : Entity object
            Object of class Entity.

        Notes
        -----
        The activity is initialized at the current simulation time. Afterwards,
        a timeout event is scheduled with duration 'init_start_delay'. When the
        generator function is resumed, the resource related to the activity is
        requested. The generator function is yielded until the requested
        resource is available. When this is the case, the activity is started
        and its duration is determined and stored in 'act_duration'. A timeout
        event is scheduled with duration 'act_duration'. Afterwards, the
        generator function reaches its end.
        """
        print(str(env.now) + ' : ' + entity.name + ' ; ' + self.name +
              ' ; initialize')
        yield env.timeout(init_start_delay)

        with self.resource.request() as req:
            yield req
            log = open('log.txt', 'a')
            print(str(env.now) + ' : ' + entity.name + ' ; ' + self.name +
                  ' ; resource ' + str(id(self.resource)) + ' ; start')
            log.write(str(env.now) + ' : ' + entity.name + ' ; ' + self.name +
                      ' ; resource ' + str(id(self.resource)) + ' ; start' +
                      '\n')
            act_duration = self.determine_duration()
            yield env.timeout(act_duration)
            print(str(env.now) + ' : ' + entity.name + ' ; ' + self.name +
                  ' ; resource ' + str(id(self.resource)) + ' ; end')
            log.write(str(env.now) + ' : ' + entity.name + ' ; ' + self.name +
                      ' ; resource ' + str(id(self.resource)) + ' ; end' +
                      '\n')
            log.close()

    def schedule_activity(self, env, init_start_delay, entity):
        """
        Scheduling function for activities

        This function schedules the activity object in the SimPy event list,
        using the activity_generator_function and the SimPy process function.

        Parameters
        ----------
        env : SimPy Environment object
            SimPy Environment object that manages the simulation time and is
            used for the scheduling and processing of events.
        init_start_delay : {0, model_instance.simulation_run_duration + 1}
            Length of delay that is taken into account after initialization.
            Value is passed by the ModelInstance object (model_instance)
            responsible for scheduling activities and other simulation model
            objects. A delay of 0 will be passed for the starting process
            activities, meaning that their execution can start immediately. For
            the other activities, 'init_start_delay' takes the value of the
            'simulation_run_duration' parameter of the ModelInstance object
            plus one. This means that the execution of the activity is
            scheduled after the end of the simulation run and hence will never
            be executed. However, when the relevant starting requirements for
            the activity are satisfied, the generator function will be resumed
            at an earlier moment in the simulation and the activity will be
            executed.
        entity : Entity object
            Object of class Entity.

        Returns
        -------
        process_event : SimPy event
            The SimPy process function returns a process event. This event is
            stored in 'process_event' and returned.

        Notes
        -----
        A generator object 'act' is created by calling the activity generator
        function. Afterwards, the environment and generator object are passed
        to a SimPy process function. The returned process event is saved in
        'process_event' and is returned.
        """
        act = self.activity_generator_function(env, init_start_delay, entity)
        process_event = env.process(act)
        return(process_event)

    def determine_duration(self):
        """
        Determine the activity duration

        This function determines the duration of the execution of the current
        activity.

        Returns
        -------
        duration : float
            The duration of the activity.

        Notes
        -----
        This function checks how the duration of the activity is defined,
        using the name of the named tuple stored in 'self.duration'. If a
        constant duration is defined, its value is passed directly to
        'duration'. When the duration is defined by a probability distribution,
        the appropriate function in the Python Random class is called to
        determine 'duration'. Finally, 'duration' is returned.
        """
        if(type(self.duration).__name__ == 'constant'):
            duration = self.duration.value
        elif(type(self.duration).__name__ == 'binomial'):
            duration = random.binomial(self.duration.trials,
                                       self.duration.probability)
        elif(type(self.duration).__name__ == 'erlang'):
            duration = random.gammavariate(self.duration.k, self.duration.k /
                                           self.duration.mean)
        elif(type(self.duration).__name__ == 'exponential'):
            duration = random.expovariate(1.0 / self.duration.mean)
        elif(type(self.duration).__name__ == 'gamma'):
            duration = random.gammavariate(self.duration.shape,
                                           self.duration.scale)
        elif(type(self.duration).__name__ == 'normal'):
            duration = random.normalvariate(self.duration.mean,
                                            self.duration.sd)
        elif(type(self.duration).__name__ == 'lognormal'):
            duration = random.lognormvariate(self.duration.mean,
                                             self.duration.sd)
        elif(type(self.duration).__name__ == 'triangular'):
            duration = random.triangular(self.duration.minim,
                                         self.duration.maxim,
                                         self.duration.mode)
        elif(type(self.duration).__name__ == 'uniform'):
            duration = random.uniform(self.duration.minim, self.duration.maxim)
        elif(type(self.duration).__name__ == 'weibull'):
            duration = random.weibullvariate(self.duration.shape,
                                             self.duration.scale)
        return(duration)

    def set_constant_duration(self, value):
        """
        Set a constant activity duration

        This function defines the duration of the activity as a constant value.

        Parameters
        ----------
        value : float
            Parameter representing the value of the constant duration.

        Notes
        -----
        This function creates a named tuple 'constant' which contains one
        value called 'value'. The created tuple is stored in the 'duration'
        attribute of the current Activity object.
        """
        constant = namedtuple('constant', 'value')
        self.duration = constant(value)

    def set_binomial_distribution(self, probability, trials):
        """
        Set a binomial distribution to model the activity duration

        This function defines the duration of the activity as following a
        binomial distribution.

        Parameters
        ----------
        probability : float
            Parameter representing the probability of success on each trial.
        trials : long
            Parameter representing the number of trials.

        Notes
        -----
        This function creates a named tuple 'binomial' which contains two
        values called 'probability' and 'trials'. The created tuple is stored
        in the 'duration' attribute of the current Activity object.
        """
        binomial = namedtuple('binomial', 'probability trials')
        self.duration = binomial(probability, trials)

    def set_erlang_distribution(self, mean, k):
        """
        Set an Erlang distribution to model the activity duration

        This function defines the duration of the activity as following an
        Erlang distribution.

        Parameters
        ----------
        mean : float
            Parameter representing the mean value.
        k : long
            Parameter representing the 'k'-value of the Erlang distribution.

        Notes
        -----
        This function creates a named tuple 'erlang' which contains two
        values called 'mean' and 'k'. The created tuple is stored in the
        'duration' attribute of the current Activity object.
        """
        erlang = namedtuple('erlang', 'mean k')
        self.duration = erlang(mean, k)

    def set_exponential_distribution(self, mean):
        """
        Set an exponential distribution to model the activity duration

        This function defines the duration of the activity as following an
        exponential distribution.

        Parameters
        ----------
        mean : float
            Parameter representing the mean value.

        Notes
        -----
        This function creates a named tuple 'exponential' which contains one
        value called 'mean'. The created tuple is stored in the 'duration'
        attribute of the current Activity object.
        """
        exponential = namedtuple('exponential', 'mean')
        self.duration = exponential(mean)

    def set_gamma_distribution(self, shape, scale):
        """
        Set a gamma distribution to model the activity duration

        This function defines the duration of the activity as following a
        gamma distribution.

        Parameters
        ----------
        shape : float
            Parameter representing the shape parameter.
        scale : float
            Parameter representing the scale parameter.

        Notes
        -----
        This function creates a named tuple 'gamma' which contains two
        values called 'shape' and 'scale'. The created tuple is stored in the
        'duration' attribute of the current Activity object.
        """
        gamma = namedtuple('gamma', 'shape scale')
        self.duration = gamma(shape, scale)

    def set_normal_distribution(self, mean, sd):
        """
        Set a normal distribution to model the activity duration

        This function defines the duration of the activity as following a
        normal distribution.

        Parameters
        ----------
        mean : float
            Parameter representing the mean.
        sd : float
            Parameter representing the standard deviation.

        Notes
        -----
        This function creates a named tuple 'normal' which contains two
        values called 'mean' and 'sd'. The created tuple is stored in the
        'duration' attribute of the current Activity object.
        """
        normal = namedtuple('normal', 'mean sd')
        self.duration = normal(mean, sd)

    def set_lognormal_distribution(self, mean, sd):
        """
        Set a lognormal distribution to model the activity duration

        This function defines the duration of the activity as following a
        lognormal distribution.

        Parameters
        ----------
        mean : float
            Parameter representing the mean.
        sd : float
            Parameter representing the standard deviation.

        Notes
        -----
        This function creates a named tuple 'lognormal' which contains two
        values called 'mean' and 'sd'. The created tuple is stored in the
        'duration' attribute of the current Activity object.
        """
        lognormal = namedtuple('lognormal', 'mean sd')
        self.duration = lognormal(mean, sd)

    def set_triangular_distribution(self, mode, minim, maxim):
        """
        Set a triangular distribution to model the activity duration

        This function defines the duration of the activity as following a
        triangular distribution.

        Parameters
        ----------
        mode : float
            Parameter representing the most likely value.
        minim : float
            Parameter representing the lower bound of the generated values.
        maxim : float
            Parameter representing the upper bound of the generated values.

        Notes
        -----
        This function creates a named tuple 'triangular' which contains three
        values called 'mode', 'minim' and 'maxim'. The created tuple is stored
        in the 'duration' attribute of the current Activity object.
        """
        triangular = namedtuple('triangular', 'minim maxim mode')
        self.duration = triangular(minim, maxim, mode)

    def set_uniform_distribution(self, minim, maxim):
        """
        Set a uniform distribution to model the activity duration

        This function defines the duration of the activity as following a
        uniform distribution.

        Parameters
        ----------
        minim : float
            Parameter representing the lower bound of the generated values.
        maxim : float
            Parameter representing the upper bound of the generated values.

        Notes
        -----
        This function creates a named tuple 'uniform' which contains two
        values called 'minim' and 'maxim'. The created tuple is stored in the
        'duration' attribute of the current Activity object.
        """
        uniform = namedtuple('uniform', 'minim maxim')
        self.duration = uniform(minim, maxim)

    def set_weibull_distribution(self, shape, scale):
        """
        Set a Weibull distribution to model the activity duration

        This function defines the duration of the activity as following a
        Weibull distribution.

        Parameters
        ----------
        shape : float
            Parameter representing the shape parameter.
        scale : float
            Parameter representing the scale parameter.

        Notes
        -----
        This function creates a named tuple 'weibull' which contains two
        values called 'shape' and 'scale'. The created tuple is stored in the
        'duration' attribute of the current Activity object.
        """
        weibull = namedtuple('weibull', 'shape scale')
        self.duration = weibull(shape, scale)

    def add_to_callback(self, process_event_other_object, model_instance):
        """
        Add the process event of the current object to a callback list

        This function makes sure that the generator function of the activity
        is resumed when a particular process event occurs
        (more specifically when 'process_event_other_object' occurs).

        Parameters
        ----------
        process_event_other_object : event
            A process event returned by the SimPy process function.
        model_instance : ModelInstance object
            Object of class ModelInstance.

        Notes
        -----
        Each event in the SimPy event list has a callback: a list of functions
        which are called when the event is processed. Using this function, the
        activity object can add the _resume function of its process event to
        the callback of 'process_event_other_object'. As each model instance
        has a process event for the current activity, the appropriate process
        event is retreived from the dictionary 'modelcomp_processev_dict' of
        'model_instance'. Note that _resume is a SimPy private function.
        """
        process_event_other_object.callbacks.append(
            model_instance.modelcomp_processev_dict[self]._resume)


class ExclusiveGateway(object):
    """
    Object that represents BPMN exclusive gateway (XOR)

    Parameters
    ----------
    name : str
        Name of the exclusive gateway.
    gateway_direction : Gateway_direction class
        Specifies requirements on he number of incoming/outgoing sequence
        flows.
    component_id: str
        Unique identification of model component in BPMN XML.

    Attributes
    ----------
    name : str
        Name of the exclusive gateway.
    gateway_direction : Gateway_direction class
        Specifies requirements on he number of incoming/outgoing sequence
        flows.
    component_id: str
        Unique identification of model component in BPMN XML.

    """

    def __init__(self, name, gateway_direction, component_id):
        self.name = name
        self.gateway_direction = gateway_direction
        self.component_id = component_id

    def exclusive_gateway_generator_function(self, env, model_instance,
                                             entity):
        """
        Exclusive gateway generator function

        This generator function models the logic of an exclusive gateway. When
        the exclusive gateway object is diverging, the conditions on the
        outgoing sequence flows are evaluated to determine which sequence flow
        should be followed to continue the process.

        Parameters
        ----------
        env : SimPy Environment object
            SimPy Environment object that manages the simulation time and is
            used for the scheduling and processing of events.
        model_instance : ModelInstance object
            Object of class ModelInstance.
        entity : Entity object
            Object of class Entity.

        Notes
        -----
        The ExclusiveGateway object is initialized at the current simulation
        time. Afterwards, a timout event is scheduled with duration
        'model_instance.simulation_model.simulation_run_duration' + 1. As a
        consequence, the process event of the ExclusiveGateway object is
        scheduled after the end of the simulation run and hence will never be
        executed. When the generator function is resumed (that is, called upon
        when the callback of the process event of another object is processed),
        the gateway is entered. When the gateway is diverging, the list of
        outgoing sequence flows of this gateway is retrieved. When the
        condition on one of these flows evaluates to True, the _resume function
        of the process event of the target object in the considered model
        instance is appended to the callback of the gateway process event of
        this model instance. The remaining conditions are not evaluated and the
        gateway is exited. When the gateway is converging, it is exited
        immediately.
        """
        print(str(env.now) + ' : ' + entity.name + ' ; ' + self.name +
              ' ; initialize gateway')
        yield env.timeout(model_instance.simulation_model.
                          simulation_run_duration + 1)
        print(str(env.now) + ' : ' + entity.name + ' ; ' + self.name +
              ' ; enter gateway')
        if (self.gateway_direction == Gateway_direction.diverging):
            sf_list = model_instance.simulation_model.gw_sf_dictionary[self]
            for i in range(len(sf_list)):
                sf = sf_list[i]
                evaluation = sf.condition
                # Above line still has to be changed using eval_condition
                if(evaluation is True):
                    model_instance.modelcomp_processev_dict[self].callbacks.\
                        append(model_instance.modelcomp_processev_dict
                               [sf.target_object]._resume)
                    break

        print(str(env.now) + ' : ' + entity.name + ' ; ' + self.name +
              ' ; exit gateway')

    def schedule_exclusive_gateway(self, env, model_instance, entity):
        """
        Scheduling function for an exclusive gateway

        This function schedules the exclusive gateway object in the SimPy event
        list, using the exclusive_gateway_generator_function and the SimPy
        process function.

        Parameters
        ----------
        env : SimPy Environment object
            SimPy Environment object that manages the simulation time and is
            used for the scheduling and processing of events.
        model_instance : ModelInstance object
            Object of class ModelInstance with attribute 'gw_sf_dictionary'.
        entity : Entity object
            Object of class Entity.

        Returns
        -------
        process_event : SimPy event
            The SimPy process function returns a process event. This event is
            stored in 'process_event' and returned.

        Notes
        -----
        A generator object 'gw' is generated by calling the exclusive gateway
        generator function. Afterwards, the environment, generator object and
        ModelInstance object are passed to a SimPy process function. The
        returned process event is saved in 'process_event' and is returned.
        """
        gw = self.exclusive_gateway_generator_function(env, model_instance,
                                                       entity)
        process_event = env.process(gw)
        return(process_event)

    def add_to_callback(self, process_event_other_object, model_instance):
        """
        Add the process event of the current object to a callback list

        This function makes sure that the generator function of the exclusive
        gateway is resumed when a particular process event occurs
        (more specifically when 'process_event_other_object' occurs).

        Parameters
        ----------
        process_event_other_object : event
            A process event returned by the SimPy process function.
        model_instance : ModelInstance object
            Object of class ModelInstance.

        Notes
        -----
        Each event in the SimPy event list has a callback: a list of functions
        which are called when the event is processed. Using this function, the
        exclusive gateway object can add the _resume function of its process
        event to the callback of 'process_event_other_object'. As each model
        instance has a process event for the current exclusive gateway, the
        appropriate process event is retreived from the dictionary
        'modelcomp_processev_dict' of 'model_instance'. Note that _resume is a
        SimPy private function.
        """
        process_event_other_object.callbacks.append(
            model_instance.modelcomp_processev_dict[self]._resume)


class InclusiveGateway(object):
    """
    Object that represents BPMN inclusive gateway (OR)

    Parameters
    ----------
    name : str
        Name of the inclusive gateway.
    gateway_direction : Gateway_direction class
        Specifies requirements on he number of incoming/outgoing sequence
        flows.
    component_id: str
        Unique identification of model component in BPMN XML.

    Attributes
    ----------
    name : str
        Name of the inclusive gateway.
    gateway_direction : Gateway_direction class
        Specifies requirements on he number of incoming/outgoing sequence
        flows.
    component_id: str
        Unique identification of model component in BPMN XML.

    """

    def __init__(self, name, gateway_direction, component_id):
        self.name = name
        self.gateway_direction = gateway_direction
        self.component_id = component_id

    def inclusive_gateway_generator_function(self, env, model_instance,
                                             entity):
        """
        Inclusive gateway generator function

        This generator function models the logic of an inclusive gateway. When
        the inclusive gateway object is diverging, the conditions on the
        outgoing sequence flows are evaluated to determine which sequence flows
        should be followed to continue the process. When the inclusive gateway
        object is converging, the function makes sure that the process only
        continues when all activities on branches enabled by the diverging
        inclusive gateway are finsihed.

        Parameters
        ----------
        env : SimPy Environment object
            SimPy Environment object that manages the simulation time and is
            used for the scheduling and processing of events.
        model_instance : ModelInstance object
            Object of class ModelInstance.
        entity : Entity object
            Object of class Entity.

        Notes
        -----
        The InclusiveGateway object is initialized at the current simulation
        time. Afterwards, a timeout event is scheduled with duration
        'model_instance.simulation_model.simulation_run_duration' + 1. As a
        consequence, the process event of the InclusiveGateway object is
        scheduled after the end of the simulation run and hence will never be
        executed. When the generator function is resumed (that is, called upon
        when the callback of the process event of another object is processed),
        the gateway is entered. When the gateway is diverging, the list of
        outgoing sequence flows of this gateway is retrieved. When the
        condition on a particular sequence flow evaluates to True, the _resume
        function of the process event of the target object in the considered
        model instance is appended to the callback of the gateway process event
        of this model instance. The number of conditions that evaluate to True
        is stored in the' ‘true_condition_counter_dict’ dictionary of the model
        instance. When the gateway is converging, the number of conditions
        which evaluated to True in the corresponding diverging gateway is
        retrieved. This value is stored in 'required_number_of_join_events' and
        indicates the number of process events from the 'converging_event_list'
        of the converging gateway in the considered model instance that need to
        occur. Using an infinite loop, a counter
        'number_of_join_events_occured' determines the number of process events
        included in the 'converging_event_list' that have been triggered. When
        'number_of_join_events_occured' corresponds to
        'required_number_of_join_events', the infinite loop is exited.
        Consequently, the gateway is exited.
        """
        print(str(env.now) + ' : ' + entity.name + ' ; ' + self.name +
              ' ; initialize gateway')
        yield env.timeout(model_instance.simulation_model.
                          simulation_run_duration + 1)
        print(str(env.now) + ' : ' + entity.name + ' ; ' + self.name +
              ' ; enter gateway')
        if (self.gateway_direction == Gateway_direction.diverging):
            model_instance.true_condition_counter_dict[self] = 0
            sf_list = model_instance.simulation_model.gw_sf_dictionary[self]
            for i in range(len(sf_list)):
                sf = sf_list[i]
                evaluation = sf.condition
                # Above line still has to be changed using eval_condition
                if(evaluation is True):
                    model_instance.modelcomp_processev_dict[self].\
                        callbacks.append(model_instance.
                                         modelcomp_processev_dict[sf.
                                                                  target_object
                                                                  ]
                                         ._resume)
                    model_instance.true_condition_counter_dict[self] += 1

        elif(self.gateway_direction == Gateway_direction.converging):
            for i in range(len(model_instance.simulation_model.
                               connected_incl_gw_list)):
                if(self in model_instance.simulation_model.
                        connected_incl_gw_list[i]):
                    diverging_gw = model_instance.simulation_model. \
                        connected_incl_gw_list[i][0]
                    break
            required_number_of_join_events = \
                model_instance.true_condition_counter_dict[diverging_gw]
            number_of_join_events_occured = 0
            converging_event_list = model_instance. \
                converging_event_list_dict[self]
            all_true_event = env.all_of(converging_event_list)

            #Create already_counted_list to support the following while-loop
            # (initialize all values to false)
            already_counted_list = []
            for i in range(len(converging_event_list)):
                already_counted_list.append(False)

            while True:
                for i in range(len(converging_event_list)):
                    if(converging_event_list[i].triggered is True
                            and already_counted_list[i] is False):
                        number_of_join_events_occured += 1
                        already_counted_list[i] = True
                if(number_of_join_events_occured ==
                        required_number_of_join_events):
                    break
                else:
                    yield all_true_event

        print(str(env.now) + ' : ' + entity.name + ' ; ' + self.name +
              ' ; exit gateway')

    def schedule_inclusive_gateway(self, env, model_instance, entity):
        """
        Scheduling function for an inclusive gateway

        This function schedules the inclusive gateway object in the SimPy event
        list, using the inclusive_gateway_generator_function and the SimPy
        process function.

        Parameters
        ----------
        env : SimPy Environment object
            SimPy Environment object that manages the simulation time and is
            used for the scheduling and processing of events.
        model_instance : ModelInstance object
            Object of class ModelInstance.
        entity : Entity object
            Object of class Entity.

        Returns
        -------
        process_event : SimPy event
            The SimPy process function returns a process event. This event is
            stored in 'process_event' and returned.

        Notes
        -----
        A generator object 'gw' is generated by calling the inclusive gateway
        generator function. Afterwards, the environment, generator object and
        ModelInstance object are passed to a SimPy process function. The
        returned process event is saved in 'process_event' and is returned.
        """
        gw = self.inclusive_gateway_generator_function(env, model_instance,
                                                       entity)
        process_event = env.process(gw)
        return(process_event)

    def add_to_callback(self, process_event_other_object, model_instance):
        """
        Add the process event of the current object to a callback list

        This function makes sure that the generator function of the inclusive
        gateway is resumed when a particular process event occurs
        (more specifically when 'process_event_other_object' occurs).

        Parameters
        ----------
        process_event_other_object : event
            A process event returned by the SimPy process function.
        model_instance : ModelInstance object
            Object of class ModelInstance.

        Notes
        -----
        Each event in the SimPy event list has a callback: a list of functions
        which are called when the event is processed. Using this function, the
        inclusive gateway object can add the _resume function of its process
        event to the callback of 'process_event_other_object'. As each model
        instance has a process event for the current inclusive gateway, the
        appropriate process event is retreived from the dictionary
        'modelcomp_processev_dict' of 'model_instance'. Note that _resume is a
        SimPy private function.
        """
        process_event_other_object.callbacks.append(
            model_instance.modelcomp_processev_dict[self]._resume)


class ParallelGateway(object):
    """
    Object that represents BPMN parallel gateway (AND)

    Parameters
    ----------
    name : str
        Name of the parallel gateway
    gateway_direction : Gateway_direction class.
        Specifies requirements on he number of incoming/outgoing sequence
        flows.
    component_id : str
        Unique identification of model component in BPMN XML.

    Attributes
    ----------
    name : str
        Name of the parallel gateway
    gateway_direction : Gateway_direction class.
        Specifies requirements on he number of incoming/outgoing sequence
        flows.
    component_id : str
        Unique identification of model component in BPMN XML.

    """

    def __init__(self, name, gateway_direction, component_id):
        self.name = name
        self.gateway_direction = gateway_direction
        self.component_id = component_id

    def parallel_gateway_generator_function(self, env, model_instance, entity):
        """
        Parallel gateway generator function

        This function models the logic of a parallel gateway. When the parallel
        gateway is converging, the function makes sure that the process only
        continues when the activities on the source of all incoming sequence
        flows are finished.

        Parameters
        ----------
        env : SimPy Environment object
            SimPy Environment object that manages the simulation time and is
            used for the scheduling and processing of events.
        model_instance : ModelInstance object
            Object of class ModelInstance.
        entity : Entity object
            Object of the class Entity.

        Notes
        -----
        The ParallelGateway object is initialized at the current simulation
        time. Afterwards, a timeout event is scheduled with duration
        'model_instance.simulation_model.simulation_run_duration' + 1. As a
        consequence, the process event of the ParallelGateway object is
        scheduled after the end of the simulation run and hence will never be
        executed. When the generator function is resumed (that is, called upon
        when the callback of the process event of another object is processed),
        the gateway is entered. When the gateway is diverging, the parallel
        gateway can be exited immediately. When the gateway is converging, an
        event 'all_of_event' is created which is processed when all of the
        events in 'converging_event_list' of the gateway in the considered
        model instance have occured. An infinite loop is started, which ends
        when 'all_of_event' is triggered. When the infinite loop is exited, the
        gateway is exited.
        """
        print(str(env.now) + ' : ' + entity.name + ' ; ' + self.name +
              ' ; initialize gateway')
        yield env.timeout(model_instance.simulation_model.
                          simulation_run_duration + 1)
        print(str(env.now) + ' : ' + entity.name + ' ; ' + self.name +
              ' ; enter gateway')

        if(self.gateway_direction == Gateway_direction.converging):
            converging_event_list = model_instance. \
                converging_event_list_dict[self]
            all_of_event = env.all_of(converging_event_list)
            while True:
                if(all_of_event.triggered is True):
                    break
                else:
                    yield all_of_event

        print(str(env.now) + ' : ' + entity.name + ' ; ' + self.name +
              ' ; exit gateway')

    def schedule_parallel_gateway(self, env, model_instance, entity):
        """
        Scheduling function for a parallel gateway

        This function schedules a parallel gateway object in the SimPy event
        list, using the parallel_gateway_generator_function and the SimPy
        process function.

        Parameters
        ----------
        env : SimPy Environment object
            SimPy Environment object that manages the simulation time and is
            used for the scheduling and processing of events.
        model : Model object
            Object of class Model.
        model_instance : ModelInstance object
            Object of class ModelInstance.
        entity : Entity object
            Object of class Entity.

        Returns
        -------
        process_event : SimPy event
            The SimPy process function returns a process event. This event is
            stored in 'process_event' and returned.

        Notes
        -----
        A generator object 'gw' is generated by calling the parallel gateway
        generator function. Afterwards, the environment, generator object and
        ModelInstance object are passed to a SimPy process function. The
        returned process event is saved in 'process_event' and is returned.
        """
        gw = self.parallel_gateway_generator_function(env, model_instance,
                                                      entity)
        process_event = env.process(gw)
        return(process_event)

    def add_to_callback(self, process_event_other_object, model_instance):
        """
        Add the process event of the current object to a callback list

        This function makes sure that the generator function of the parallel
        gateway is resumed when a particular process event occurs
        (more specifically when 'process_event_other_object' occurs).

        Parameters
        ----------
        process_event_other_object : event
            A process event returned by the SimPy process function.
        model_instance : ModelInstance object
            Object of class ModelInstance.

        Notes
        -----
        Each event in the SimPy event list has a callback: a list of functions
        which are called when the event is processed. Using this function, the
        parallel gateway object can add the _resume function of its process
        event to the callback of 'process_event_other_object'. As each model
        instance has a process event for the current parallel gateway, the
        appropriate process event is retreived from the dictionary
        'modelcomp_processev_dict' of 'model_instance'. Note that _resume is a
        SimPy private function.
        """
        process_event_other_object.callbacks.append(
            model_instance.modelcomp_processev_dict[self]._resume)


class SequenceFlow(object):
    """
    Object that represents BPMN sequence flow

    Parameters
    ----------
    name : str
        Name of the sequence flow.
    source_node : str
        Unique 'component_id' of the source node of the sequence flow.
    target_node : str
        Unique 'component id' of the target node of the sequence flow.
    component_id: str
        Unique identification of model component in BPMN XML.

    Attributes
    ----------
    name : str
        Name of the sequence flow.
    source_node : str
        Unique 'component_id' of the source node of the sequence flow.
    target_node : str
        Unique 'component id' of the target node of the sequence flow.
    component_id: str
        Unique identification of model component in BPMN XML.
    condition : function
        A function which returns True when the condition holds or false
        when the evaluation of the condition fails. Initialized to None.
    source_object : object
        Object from which the sequence flow originates. This is the object
        with 'source_node' as its 'component_id'.
    target_object : object
        Object to which the sequence flow is directed. This is the object
        with 'source_node' as its 'component_id'.

    """

    def __init__(self, name, source_node, target_node, component_id):
        self.name = name
        self.source_node = source_node
        self.target_node = target_node
        self.component_id = component_id
        self.condition = None
        self.source_object = None
        self.target_object = None

    def set_condition(self, condition):
        """
        Add a condition to the sequence flow

        The condition must evaluate to True, in order for the target_node to be
        enabled. Note that the condition that the source_node must be completed
        should not be explicitly modelled as a condition.

        Parameters
        ----------
        condition : function
            A function which returns True when the condition holds or false
            when the evaluation of the condition fails.
        """
        self.condition = condition

    def eval_condition(self, **kwargs):
        """
        Evaluate the condition of the sequence flow

        Parameters
        ----------
        **kwargs : list of named arguments
            The list of named arguments needed by the condition function.

        Returns
        -------
        condition : function
            Returns the condition function with values for its named arguments.
        """
        return self.condition(**kwargs)


class Entity(object):
    """
    Object that represents an entity

    Parameters
    ----------
    name : str
        Name of the entity.

    """

    def __init__(self, name):
        self.name = name


class ModelInstance(object):
    """
    Object representing an instance of the defined SimulationModel object

    Parameters
    ----------
    simulation_model : SimulationModel object
        Object of class SimulationModel.

    Attributes
    ----------
    simulation_model : SimulationModel object
        Object of class SimulationModel.
    modelcomp_processev_dict : dictionary
        Dictionary mapping the Activity, ExclusiveGateway, InclusiveGateway and
        ParallelGateway objects to their associated process event of the
        current ModelInstance object.
    converging_event_list_dict : dictionary
        Dictionary mapping the InclusiveGateway and ParallelGateway objects to
        their associated converging event list of the current ModelInstance
        object. The converging event list is a list composed of the activity
        process events which contain the _resume function of the converging
        inclusive or parallel gateway process event in their callback.
    true_condition_counter_dict : dictionary
        Dictionary mapping the diverging InclusiveGateway objects to the number
        of conditions on their outgoing sequence flow which evalutate to True
        for the current ModelInstance object.

    """

    def __init__(self, simulation_model):
        self.simulation_model = simulation_model
        self.modelcomp_processev_dict = dict()
        self.converging_event_list_dict = dict()
        self.true_condition_counter_dict = dict()

    def specify_model_instance(self, env, entity):
        """
        Specify the model instance

        This function specifies the model instance by calling several other
        functions. In particular, the activities and gateways of the model
        instance are scheduled and appropriate callback additions are
        effectuated.

        Parameters
        ----------
        env : SimPy Environment object
            SimPy Environment object that manages the simulation time and is
            used for the scheduling and processing of events.
        entity : Entity object
            Object of class Entity.

        Notes
        -----
        This function merely calls upon several other functions. Firstly,
        activites and gateways are scheduled for the particular model instance.
        Secondly, a function is called which generates the converging event
        lists for InclusiveGateway and ParallelGateway objects. Finally,
        several functions are called upon in order to append the appropriate
        _resume functions of process events to the callback of other process
        events.
        """
        self.schedule_instance_activities(env, entity)
        self.schedule_instance_parallel_gateway(env, entity)
        self.schedule_instance_exclusive_gateway(env, entity)
        self.schedule_instance_inclusive_gateway(env, entity)
        self.generate_converging_event_lists()
        self.add_activities_to_callbacks()
        self.add_parallel_gateways_to_callbacks()
        self.add_exclusive_gateways_to_callbacks()
        self.add_inclusive_gateways_to_callbacks()

    def schedule_instance_activities(self, env, entity):
        """
        Schedule the activities for a particular model instance

        This function enables the scheduling of the activities of a particular
        model instance in the SimPy event list.

        Parameters
        ----------
        env : SimPy Environment object
            SimPy Environment object that manages the simulation time and is
            used for the scheduling and processing of events.
        entity : Entity object
            Object of class Entity.

        Notes
        -----
        This function sequentially considers each activity defined in the
        simulation model. When the activity is a starting activity of the
        model (i.e. if the considered Activity object is included in the
        'starting_activity_set' of the SimulationModel object), the
        schedule_activity function of class Activity is called upon with an
        'init_start_delay' of 0 (i.e. the activity can start at the current
        simulation time). Otherwise, an 'initial_start_delay' of the length
        of the simulation run + 1 is passed. The returned process event 'pe'
        is stored as a value in the 'modelcomp_processev_dict' dictionary,
        associated to the Activity object as a key.
        """
        for i in range(len(self.simulation_model.activity_list)):
            activity = self.simulation_model.activity_list[i]
            if(activity in self.simulation_model.starting_activity_set):
                pe = activity.schedule_activity(env, 0, entity)
            else:
                pe = activity.schedule_activity(env, self.simulation_model.
                                                simulation_run_duration + 1,
                                                entity)
            self.modelcomp_processev_dict[activity] = pe

    def schedule_instance_parallel_gateway(self, env, entity):
        """
        Schedule the parallel gateways for a particular model instance

        This function enables the scheduling of the parallel gateways of a
        particular model instance in the SimPy event list.

        Parameters
        ----------
        env : SimPy Environment object
            SimPy Environment object that manages the simulation time and is
            used for the scheduling and processing of events.
        entity : Entity object
            Object of class Entity.

        Notes
        -----
        This function sequentially considers each parallel gateway defined in
        the simulation model. For each ParallelGateway object, the
        schedule_parallel_gateway function of class ParallelGateway is called
        upon. The returned process event 'pe' is stored as a value in the
        'modelcom_processev_dict' dictionary, associated to the
        ParallelGateway object as a key.
        """
        for i in range(len(self.simulation_model.parallel_gateway_list)):
            parallel_gateway = self.simulation_model.parallel_gateway_list[i]
            pe = parallel_gateway.schedule_parallel_gateway(env, self, entity)
            self.modelcomp_processev_dict[parallel_gateway] = pe

    def schedule_instance_exclusive_gateway(self, env, entity):
        """
        Schedule the exclusive gateways for a particular model instance

        This function enables the scheduling of the exclusive gateways of a
        particular model instance in the SimPy event list.

        Parameters
        ----------
        env : SimPy Environment object
            SimPy Environment object that manages the simulation time and is
            used for the scheduling and processing of events.
        entity : Entity object
            Object of class Entity.

        Notes
        -----
        This function sequentially considers each exclusive gateway defined in
        the simulation model. For each ExclusiveGateway object, the
        schedule_parallel_gateway function of class ExclusiveGateway is called
        upon. The returned process event 'pe' is stored as a value in the
        'modelcom_processev_dict' dictionary, associated to the
        ExclusiveGateway object as a key.
        """
        for i in range(len(self.simulation_model.exclusive_gateway_list)):
            exclusive_gateway = self.simulation_model.exclusive_gateway_list[i]
            pe = exclusive_gateway.schedule_exclusive_gateway(env, self,
                                                              entity)
            self.modelcomp_processev_dict[exclusive_gateway] = pe

    def schedule_instance_inclusive_gateway(self, env, entity):
        """
        Schedule the inclusive gateways for a particular model instance

        This function enables the scheduling of the inclusive gateways of a
        particular model instance in the SimPy event list.

        Parameters
        ----------
        env : SimPy Environment object
            SimPy Environment object that manages the simulation time and is
            used for the scheduling and processing of events.
        entity : Entity object
            Object of class Entity.

        Notes
        -----
        This function sequentially considers each inclusive gateway defined in
        the simulation model. For each InclusiveGateway object, the
        schedule_parallel_gateway function of class InclusiveGateway is called
        upon. The returned process event 'pe' is stored as a value in the
        'modelcom_processev_dict' dictionary, associated to the
        InclusiveGateway object as a key.
        """
        for i in range(len(self.simulation_model.inclusive_gateway_list)):
            inclusive_gateway = self.simulation_model.inclusive_gateway_list[i]
            pe = inclusive_gateway.schedule_inclusive_gateway(env, self,
                                                              entity)
            self.modelcomp_processev_dict[inclusive_gateway] = pe

    def add_activities_to_callbacks(self):
        """
        Allow activities to insert themselves in the appropriate callbacks

        This function allows activities to insert the _resume function of their
        process event to the callback of another process event. During the
        simulation run, this leads to the resumption of the activity when the
        relevant preceding activities/gateways have terminated.

        Notes
        -----
        This function enables activities to insert the _resume function of
        their process event to the callback of other process events. This is
        effectuated by sequentially considering each activity defined in the
        simulation model. If the considered activity is a starting activity,
        there are no preceding activities or gateways to which process event
        the current activity might add the _resume function of its process
        event to. Otherwise, the _resume function of the activity's process
        event will be added to the callback of the source objects of incoming
        sequence flows (this can be activities or gateways). One exception
        should be taken into account: if the source object is a diverging
        exclusive or inclusive gateway, addition to callback is left over to
        the logic embedded in  the gateway.
        """
        for i in range(len(self.simulation_model.activity_list)):
            activity = self.simulation_model.activity_list[i]
            if(activity not in self.simulation_model.starting_activity_set):
                source_object_list = \
                    self.simulation_model. \
                    source_obj_incoming_sf_dictionary[activity]
                for j in range(len(source_object_list)):
                    source_object = source_object_list[j]
                    if not((source_object.__class__.__name__ ==
                            'ExclusiveGateway'
                            and source_object.gateway_direction ==
                            Gateway_direction.diverging)
                            or (source_object.__class__.__name__ ==
                                'InclusiveGateway'
                                and source_object.gateway_direction ==
                                Gateway_direction.diverging)):
                        activity.add_to_callback(
                            self.modelcomp_processev_dict[source_object], self)

    def add_parallel_gateways_to_callbacks(self):
        """
        Allow parallel gateways to insert themselves in the appropriate
        callbacks

        This function allows parallel gateways to insert the _resume function
        of their process event to the callback of another process event. During
        the simulation run, this leads to the resumption of the parallel
        gateway when the relevant preceding activities/gateways have
        terminated.

        Notes
        -----
        This function enables parallel gateways to insert the _resume function
        of their process event to the callback of other process events. This is
        effectuated by sequentially considering each parallel gateway defined
        in the simulation model. The _resume function of the parallel gateway's
        process event will be added to the callback of the source objects of
        incoming sequence flows (this can be activities or gateways). One
        exception should be taken into account: if the source object is a
        diverging exclusive or inclusive gateway, addition to callback is left
        over to the logic embedded in  the gateway.
        """
        for i in range(len(self.simulation_model.parallel_gateway_list)):
            parallel_gateway = self.simulation_model.parallel_gateway_list[i]
            source_object_list = self.simulation_model.\
                source_obj_incoming_sf_dictionary[parallel_gateway]
            for j in range(len(source_object_list)):
                source_object = source_object_list[j]
                if not((source_object.__class__.__name__ ==
                        'ExclusiveGateway'
                        and source_object.gateway_direction ==
                        'diverging')
                        or (source_object.__class__.__name__ ==
                            'InclusiveGateway'
                            and source_object.gateway_direction ==
                            'diverging')):
                    parallel_gateway.add_to_callback(
                        self.modelcomp_processev_dict[source_object], self)

    def add_exclusive_gateways_to_callbacks(self):
        """
        Allow exclusive gateways to insert themselves in the appropriate
        callbacks

        This function allows exclusive gateways to insert the _resume function
        of their process event to the callback of another process event. During
        the simulation run, this leads to the resumption of the exclusive
        gateway when the relevant preceding activities/gateways have
        terminated.

        Notes
        -----
        This function enables exclusive gateways to insert the _resume function
        of their process event to the callback of other process events. This is
        effectuated by sequentially considering each exclusive gateway defined
        in the simulation model. The _resume function of the exclusive
        gateway's process event will be added to the callback of the source
        objects of incoming sequence flows (this can be activities or
        gateways). One exception should be taken into account: if the source
        object is a diverging exclusive or inclusive gateway, addition to
        callback is left over to the logic embedded in  the gateway.
        """
        for i in range(len(self.simulation_model.exclusive_gateway_list)):
            exclusive_gateway = self.simulation_model.exclusive_gateway_list[i]
            source_object_list = self.simulation_model.\
                source_obj_incoming_sf_dictionary[exclusive_gateway]
            for j in range(len(source_object_list)):
                source_object = source_object_list[j]
                if not((source_object.__class__.__name__ ==
                        'ExclusiveGateway'
                        and source_object.gateway_direction ==
                        Gateway_direction.diverging)
                        or (source_object.__class__.__name__ ==
                            'InclusiveGateway'
                            and source_object.gateway_direction ==
                            Gateway_direction.diverging)):
                    exclusive_gateway.add_to_callback(
                        self.modelcomp_processev_dict[source_object], self)

    def add_inclusive_gateways_to_callbacks(self):
        """
        Allow inclusive gateways to insert themselves in the appropriate
        callbacks

        This function allows inclusive gateways to insert the _resume function
        of their process event to the callback of another process event. During
        the simulation run, this leads to the resumption of the inclusive
        gateway when the relevant preceding activities/gateways have
        terminated.

        Notes
        -----
        This function enables inclusive gateways to insert the _resume function
        of their process event to the callback of other process events. This is
        effectuated by sequentially considering each inclusive gateway defined
        in the simulation model. The _resume function of the inclusive
        gateway's process event will be added to the callback of the source
        objects of incoming sequence flows (this can be activities or
        gateways). One exception should be taken into account: if the source
        object is a diverging exclusive or inclusive gateway, addition to
        callback is left over to the logic embedded in  the gateway.
        """
        for i in range(len(self.simulation_model.inclusive_gateway_list)):
            inclusive_gateway = self.simulation_model.inclusive_gateway_list[i]
            source_object_list = self.simulation_model.\
                source_obj_incoming_sf_dictionary[inclusive_gateway]

            for j in range(len(source_object_list)):
                source_object = source_object_list[j]
                if not((source_object.__class__.__name__ ==
                        'ExclusiveGateway'
                        and source_object.gateway_direction ==
                        Gateway_direction.diverging)
                        or (source_object.__class__.__name__ ==
                            'InclusiveGateway'
                            and source_object.gateway_direction ==
                            Gateway_direction.diverging)):
                    inclusive_gateway.add_to_callback(
                        self.modelcomp_processev_dict[source_object], self)

    def generate_converging_event_lists(self):
        """
        Generate converging event lists for parallel and inclusive gateways

        This function composes a converging event list for each converging
        parallel or inclusive getaway. This list will support the gateway logic
        by giving an overview of the process events of the source nodes of the
        converging gateway's incoming sequence flows.

        Notes
        -----
        This function generates a converging event list for converging parallel
        and inclusive gateways. The generated list includes the process events
        of model components (activities or gateways) that are located at the
        source of the incoming sequence flows of the considered gateway. The
        obtained converging event list is stored in the 'converging_event_list
        _dict' dictionary of the model_instance object with gateway object as
        a key and the corresponding converging event list as a value.
        """
        for i in range(len(self.simulation_model.parallel_gateway_list)):
            parallel_gateway = self.simulation_model.parallel_gateway_list[i]

            if parallel_gateway.gateway_direction == \
                    Gateway_direction.converging:
                source_object_list = self.simulation_model.\
                    source_obj_incoming_sf_dictionary[parallel_gateway]
                converging_event_list = []
                for j in range(len(source_object_list)):
                    source_object = source_object_list[j]
                    converging_event_list.append(self.modelcomp_processev_dict
                                                 [source_object])
                self.converging_event_list_dict[parallel_gateway] = \
                    converging_event_list

        for i in range(len(self.simulation_model.inclusive_gateway_list)):
            inclusive_gateway = self.simulation_model.inclusive_gateway_list[i]

            if inclusive_gateway.gateway_direction == \
                    Gateway_direction.converging:
                source_object_list = self.simulation_model.\
                    source_obj_incoming_sf_dictionary[inclusive_gateway]
                converging_event_list = []
                for j in range(len(source_object_list)):
                    source_object = source_object_list[j]
                    converging_event_list.append(self.modelcomp_processev_dict
                                                 [source_object])
                self.converging_event_list_dict[inclusive_gateway] = \
                    converging_event_list


class SimulationModel(object):
    """
    Object representing the simulation model (basic structure and features)

    Attributes
    ----------
    simulation_run_duration : float
        Duration of a simulation run.
    activity_list : list of Activity objects
        List containing the Activity objects created in the model.
    exclusive_gateway_list : list of ExclusiveGateway objects
        List containing the ExclusiveGateway objects created in the model.
    inclusive_gateway_list : list of InclusiveGateway objects
        List containing the InclusiveGateway objects created in the model.
    parallel_gateway_list : list of ParallelGateway objects
        List containing the ParallelGateway objects created in the model.
    sequence_flow_list : list of SequenceFlow objects
        List containing the SequenceFlow objects created in the model.
    id_obj_dictionary : dictionary
        Dictionary mapping unique 'component_id' to an Activity,
        ExclusiveGateway, InclusiveGateway or ParallelGateway object. A
        'component_id' is the identification of a model component in BPMN XML
        and functions as a key. The corresponding object is its value.
    source_obj_dictionary : dictionary
        Dictionary mapping Activity, ExclusiveGateway, InclusiveGateway and
        ParallelGateway objects (keys) to the 'source_object' of the incoming
        sequence flow or sequence flows (values).
    gw_sf_dictionary : dictionary
        Dictionary mapping diverging ExlusiveGateway and InclusiveGateway
        objects (keys) to the outgoing sequence flows (values).
    connected_incl_gw_list : list of connected InclusiveGateway objects
        List of which each element represents a diverging InclusiveGateway
        object and its corresponding converging InclusiveGateway object.
    starting_activity_set : set of Activity objects
        Set containing the starting activity or activities of the model.

    """

    def __init__(self):
        self.simulation_run_duration = None
        self.activity_list = []
        self.exclusive_gateway_list = []
        self.inclusive_gateway_list = []
        self.parallel_gateway_list = []
        self.sequence_flow_list = []
        self.id_obj_dictionary = dict()
        self.source_obj_incoming_sf_dictionary = dict()
        self.gw_sf_dictionary = dict()
        self.connected_incl_gw_list = []
        self.starting_activity_set = []

    def simulate(self, simulation_run_duration):
        """
        Run the composed simulation model

        This function runs the composed simulation model for a particular
        period of time.

        Parameters
        ----------
        simulation_run_duration : float
            Duration of a simulation run.

        Notes
        -----
        This function creates a SimPy simulation environment in which, amongst
        others, the event list will be stored. Moreover,
        simulation_model_generator_function is called upon to create a
        generator object. This object is processed using the SimPy process
        function. Afterwards, the file which will contain the event log is
        created. Finally, the run of the simulation model is started for the
        duration 'simulation_run_duration'.
        """
        self.simulation_run_duration = simulation_run_duration
        env = simpy.Environment()
        simulation_model_generator_object = \
            self.simulation_model_generator_function(env)
        env.process(simulation_model_generator_object)

        log = open('log.txt', 'w')
        log.write('EVENT LOG' + '\n' + '\n')
        log.close()

        env.run(until=self.simulation_run_duration)

    def simulation_model_generator_function(self, env):
        """
        Simulation model generator function

        This function enables the specification of the simulation model and the
        definition of multiple model instances and entities.

        Parameters
        ----------
        env : SimPy Environment object
            SimPy Environment object that manages the simulation time and is
            used for the scheduling and processing of events.

        Notes
        -----
        This function calls upon the specify_simulation_model function,
        enabling the specification of the base structure of the simulation
        model. Afterwards, a function enabling the generation of some
        supporting artifacts is called upon. Finally, several entities and
        instances of the model are generated. Between the generation of an
        entity and model instance, a particular interarrival time is taken into
        account.
        """
        self.specify_simulation_model(env)
        self.generate_simulation_model_supporting_artifacts()

        for entity_id in range(1, 3):
            model_instance = ModelInstance(self)
            entity = Entity('entity ' + str(entity_id))
            model_instance.specify_model_instance(env, entity)
            interarrival_time = 6
            yield env.timeout(interarrival_time)

    def specify_simulation_model(self, env):
        """
        Specify the basic structure of the simulation model

        This function specifies the basic structure of the simulation model:
        its activities, gateways, sequence flows and resources.

        Parameters
        ----------
        env : SimPy Environment object
            SimPy Environment object that manages the simulation time and is
            used for the scheduling and processing of events.

        Notes
        -----
        This function creates the Activity, InclusiveGateway, ExclusiveGateway,
        ParallelGateway and SequenceFlow objects using their respective
        creation functions. For the Activity objects, the appropriate
        probability distribution or value for their duration is defined. For
        inclusive gateways, the corresponding diverging and converging gateway
        are connected by calling the connect_inclusive_gateway function.
        Finally, the SimPy Resource class is used to define resources, which
        are afterwards assigned to the activities.
        """
        A = self.create_activity('activity_A', 'acA')
        B = self.create_activity('activity_B', 'acB')
        C = self.create_activity('activity_C', 'acC')
        D = self.create_activity('activity_D', 'acD')
        E = self.create_activity('activity_E', 'acE')
        F = self.create_activity('activity_F', 'acF')
        G = self.create_activity('activity_G', 'acG')
        H = self.create_activity('activity_H', 'acH')
        I = self.create_activity('activity_I', 'acI')
        J = self.create_activity('activity_J', 'acJ')

        A.set_triangular_distribution(12, 5, 20)
        B.set_triangular_distribution(20, 1, 40)
        C.set_triangular_distribution(12, 10, 30)
        D.set_triangular_distribution(22, 10, 50)
        E.set_constant_duration(10)
        F.set_constant_duration(4)
        G.set_constant_duration(8)
        H.set_constant_duration(22)
        I.set_constant_duration(7)
        J.set_constant_duration(18)

        Split1 = self.create_inclusive_gateway('OR_split1',
                                               Gateway_direction.diverging,
                                               'splitGW1')
        Split2 = self.create_exclusive_gateway('XOR_split2',
                                               Gateway_direction.diverging,
                                               'splitGW2')
        Split3 = self.create_parallel_gateway('AND_split3',
                                              Gateway_direction.diverging,
                                              'splitGW3')
        Join1 = self.create_inclusive_gateway('OR_join1',
                                              Gateway_direction.converging,
                                              'joinGW1')
        Join2 = self.create_exclusive_gateway('XOR_join2',
                                              Gateway_direction.converging,
                                              'joinGW2')
        Join3 = self.create_parallel_gateway('AND_join3',
                                             Gateway_direction.converging,
                                             'joinGW3')
        
        sf1 = self.create_sequence_flow('sequenceflow_1', 'acA', 'splitGW1',
                                        'sf1')
        sf2 = self.create_sequence_flow('sequenceflow_2', 'splitGW1', 'acB',
                                        'sf2')
        sf2.condition = True
        sf3 = self.create_sequence_flow('sequenceflow_3', 'acB', 'splitGW2',
                                        'sf3')
        sf4 = self.create_sequence_flow('sequenceflow_4', 'splitGW2', 'acC',
                                        'sf4')
        sf4.condition = True
        sf5 = self.create_sequence_flow('sequenceflow_5', 'acC', 'acE',
                                        'sf5')
        sf6 = self.create_sequence_flow('sequenceflow_6', 'acE', 'joinGW2',
                                        'sf6')
        sf7 = self.create_sequence_flow('sequenceflow_7', 'splitGW2', 'acD',
                                        'sf7')
        sf7.condition = True
        sf8 = self.create_sequence_flow('sequenceflow_8', 'acD', 'joinGW2',
                                        'sf8')
        sf9 = self.create_sequence_flow('sequenceflow_9', 'splitGW1',
                                        'splitGW3', 'sf9')
        sf9.condition = True
        sf10 = self.create_sequence_flow('sequenceflow_10', 'splitGW3', 'acF',
                                         'sf10')
        sf11 = self.create_sequence_flow('sequenceflow_11', 'splitGW3', 'acG',
                                         'sf11')
        sf12 = self.create_sequence_flow('sequenceflow_12', 'splitGW3', 'acH',
                                         'sf12')
        sf13 = self.create_sequence_flow('sequenceflow_13', 'acF', 'joinGW3',
                                         'sf13')
        sf14 = self.create_sequence_flow('sequenceflow_14', 'acG', 'joinGW3',
                                         'sf14')
        sf15 = self.create_sequence_flow('sequenceflow_15', 'acH', 'joinGW3',
                                         'sf15')
        sf16 = self.create_sequence_flow('sequenceflow_16', 'joinGW3', 'acI',
                                         'sf16')
        sf17 = self.create_sequence_flow('sequenceflow_17', 'acI', 'joinGW1',
                                         'sf17')
        sf18 = self.create_sequence_flow('sequenceflow_18', 'joinGW1', 'acJ',
                                         'sf18')
        sf19 = self.create_sequence_flow('sequenceflow_19', 'joinGW2',
                                         'joinGW1', 'sf19')

        self.connect_inclusive_gateways(Split1, Join1)

        res1 = simpy.Resource(env, capacity=1)
        res2 = simpy.Resource(env, capacity=1)
        res3 = simpy.Resource(env, capacity=1)
        res4 = simpy.Resource(env, capacity=1)
        res5 = simpy.Resource(env, capacity=1)
        res6 = simpy.Resource(env, capacity=1)
        res7 = simpy.Resource(env, capacity=1)
        res8 = simpy.Resource(env, capacity=1)
        res9 = simpy.Resource(env, capacity=1)
        res10 = simpy.Resource(env, capacity=1)

        self.assign_resource(res1, A)
        self.assign_resource(res2, B)
        self.assign_resource(res3, C)
        self.assign_resource(res4, D)
        self.assign_resource(res5, E)
        self.assign_resource(res6, F)
        self.assign_resource(res7, G)
        self.assign_resource(res8, H)
        self.assign_resource(res9, I)
        self.assign_resource(res10, J)

    def create_activity(self, name, component_id):
        """
        Create an Activity object

        Function that creates an activity of the simulation model and adds it
        to a supporting dictionary and list.

        Parameters
        ----------
        name : str
            Name of the activity.
        component_id: str
            Unique identification of model component in BPMN XML.

        Returns
        -------
        act : Activity object
            Created Activity object is returned.

        Notes
        -----
        This function creates an Activity object by passing the appropriate
        parameters to its constructor. The created object is afterwards added
        as a value to a dictionary entry in 'id_obj_dictionary' with
        'component_id' as a key. The list 'activity_list' is updated by adding
        the Activity object. Finally, the Activity object is returned.
        """
        act = Activity(name, component_id)
        self.id_obj_dictionary[component_id] = act
        self.activity_list.append(act)
        return(act)

    def create_exclusive_gateway(self, name, gateway_direction, component_id):
        """
        Create a ExclusiveGateway object

        Function that creates an exclusive gateway of the simulation model and
        adds it to a supporting dictionary and list.

        Parameters
        ----------
        name : str
            Name of the exclusive gateway
        gateway_direction : Gateway_direction class.
            Specifies requirements on he number of incoming/outgoing sequence
            flows.
        component_id : str
            Unique identification of model component in BPMN XML.

        Returns
        -------
        XOR_gw : ExclusiveGateway object
            Created ExclusiveGateway object is returned.
        
        Notes
        -----
        This function creates an ExclusiveGateway object by passing the
        appropriate parameters to its constructor. The created object is
        afterwards added as a value to a dictionary entry in
        'id_obj_dictionary' with 'component_id' as a key. The list
        'exclusive_gateway_list' is updated by adding the ExclusiveGateway
        object. Finally, the ExclusiveGateway object is returned.
        """
        XOR_gw = ExclusiveGateway(name, gateway_direction, component_id)
        self.id_obj_dictionary[component_id] = XOR_gw
        self.exclusive_gateway_list.append(XOR_gw)
        return(XOR_gw)

    def create_inclusive_gateway(self, name, gateway_direction, component_id):
        """
        Create a InclusiveGateway object

        Function that creates an inclusive gateway of the simulation model and
        adds it to a supporting dictionary and list.

        Parameters
        ----------
        name : str
            Name of the inclusive gateway
        gateway_direction : Gateway_direction class.
            Specifies requirements on he number of incoming/outgoing sequence
            flows.
        component_id : str
            Unique identification of model component in BPMN XML.

        Returns
        -------
        OR_gw : InclusiveGateway object
            Created InclusiveGateway object is returned.
        
        Notes
        -----
        This function creates an InclusiveGateway object by passing the
        appropriate parameters to its constructor. The created object is
        afterwards added as a value to a dictionary entry in
        'id_obj_dictionary' with 'component_id' as a key. The list
        'exclusive_gateway_list' is updated by adding the InclusiveGateway
        object. Finally, the InclusiveGateway object is returned.
        """
        OR_gw = InclusiveGateway(name, gateway_direction, component_id)
        self.id_obj_dictionary[component_id] = OR_gw
        self.inclusive_gateway_list.append(OR_gw)
        return(OR_gw)

    def create_parallel_gateway(self, name, gateway_direction, component_id):
        """
        Create a ParallelGateway object

        Function that creates a parallel gateway of the simulation model and
        adds it to a supporting dictionary and list.

        Parameters
        ----------
        name : str
            Name of the parallel gateway
        gateway_direction : Gateway_direction class.
            Specifies requirements on he number of incoming/outgoing sequence
            flows.
        component_id : str
            Unique identification of model component in BPMN XML.

        Returns
        -------
        AND_gw : ParallelGateway object
            Created ParallelGateway object is returned.

        Notes
        -----
        This function creates an ParalleleGateway object by passing the
        appropriate parameters to its constructor. The created object is
        afterwards added as a value to a dictionary entry in
        'id_obj_dictionary' with 'component_id' as a key. The list
        'exclusive_gateway_list' is updated by adding the ParallelGateway
        object. Finally, the ParallelGateway object is returned.
        """
        AND_gw = ParallelGateway(name, gateway_direction, component_id)
        self.id_obj_dictionary[component_id] = AND_gw
        self.parallel_gateway_list.append(AND_gw)
        return(AND_gw)

    def create_sequence_flow(self, name, source_node, target_node,
                             component_id):
        """
        Create a SequenceFlow object

        Function that creates a sequence flow of the simulation model and
        adds it to a supporting list.

        Parameters
        ----------
        name : str
            Name of the sequence flow.
        source_node : str
            Unique 'component_id' of the source node of the sequence flow.
        target_node : str
            Unique 'component id' of the target node of the sequence flow.
        component_id: str
            Unique identification of model component in BPMN XML.

        Returns
        -------
        sf : SequenceFlow object
            Created SequenceFlow object is returned.

        Notes
        -----
        This function creates an SequenceFlow object by passing the
        appropriate parameters to its constructor. The list
        'sequence_flow_list' is updated by adding the SequenceFlow object.
        Afterwards, the SequenceFlow object is returned.
        """
        sf = SequenceFlow(name, source_node, target_node, component_id)
        self.sequence_flow_list.append(sf)
        return(sf)

    def assign_resource(self, resource, activity):
        """
        Assign a resource to an activity

        Function that assigns a resource to an activity it is supposed to
        perform.

        Parameters
        ----------
        resource : SimPy Resource object
            Object of the SimPy Resource class.

        Notes
        -----
        This function assigns 'resource' to 'activity' by storing 'resource' in
        the 'activity' attribute with the same name.
        """
#        activity.resource_list.append(resource)
        activity.resource = resource

    def generate_simulation_model_supporting_artifacts(self):
        """
        Generate supporting simulation model artifacts

        This function calls upon several other functions, which generate
        supporting artifacts related to the simulation model.

        Notes
        -----
        This function calls upon functions which create two supporting
        dictionaries and a supporting set. These artifacts are stored as
        attributes of the SimulationModel object and are called upon by several
        other functions in several classes.
        """
        self.source_obj_incoming_sf_dictionary = \
            self.generate_source_obj_incoming_sf_dictionary()
        self.gw_sf_dictionary = self.generate_gw_sf_dictionary()
        self.starting_activity_set = self.generate_starting_activity_set()

    def generate_source_obj_incoming_sf_dictionary(self):
        """
        Generate a dictionary to map an object to the source objects of its
        incoming sequence flows

        This function generates a dictionary which maps an Activity,
        ParallelGateway, ExclusiveGateway or InclusiveGateway object to the
        source objects of their incoming sequence flow or sequence flows.

        Returns
        -------
        source_obj_dictionary : dictionary
            Dictionary mapping Activity, ExclusiveGateway, InclusiveGateway and
            ParallelGateway objects (keys) to the 'source_object' of the
            incoming sequence flow or sequence flows (values).

        Notes
        -----
        The information to generate the dictionary is obtained by enmerating
        the sequence flows included in the 'sequence_flow_list' of the Model
        object. Moreover, 'source_object' and 'target_object' are saved in the
        corresponding characteristics of the SequenceFlow object, as a
        reference to the object is more informative than a reference to the
        'component_id' (one of the attributes of the object).
        """
        source_obj_incoming_sf_dictionary = dict()

        for i in range(len(self.sequence_flow_list)):
            sequence_flow = self.sequence_flow_list[i]
            source_object = self.id_obj_dictionary[sequence_flow.source_node]
            sequence_flow.source_object = source_object
            target_object = self.id_obj_dictionary[sequence_flow.target_node]
            sequence_flow.target_object = target_object

            if(target_object.__class__.__name__ == 'Activity'
                    or target_object.__class__.__name__ == 'ParallelGateway'
                    or target_object.__class__.__name__ == 'ExclusiveGateway'
                    or target_object.__class__.__name__ == 'InclusiveGateway'):
                if(target_object not in source_obj_incoming_sf_dictionary):
                    source_obj_incoming_sf_dictionary.setdefault(target_object,
                                                                 [])
                    source_obj_incoming_sf_dictionary[target_object].append(
                        sequence_flow.source_object)
                else:
                    source_obj_incoming_sf_dictionary[target_object].append(
                        sequence_flow.source_object)

        return(source_obj_incoming_sf_dictionary)

    def generate_gw_sf_dictionary(self):
        """
        Generate a dictionary to map a diverging exclusive or inclusive gateway
        to their outgoing sequence flows

        This function generates a dictionary which maps an ExclusiveGateway
        or InclusiveGateway object to SequenceFlow objects having these
        gateways as a 'source_node'.

        Returns
        -------
        gw_sf_dictionary : dictionary
            Dictionary mapping diverging ExlusiveGateway and InclusiveGateway
            objects (keys) to the outgoing sequence flows (values).

        Notes
        -----
        The information to generate the dictionary is obtained by enmerating
        the sequence flows included in the 'sequence_flow_list' of the Model
        object.
        """
        gw_sf_dictionary = dict()

        for i in range(len(self.sequence_flow_list)):
            sequence_flow = self.sequence_flow_list[i]
            source_object = self.id_obj_dictionary[sequence_flow.source_node]

            if((source_object.__class__.__name__ == 'ExclusiveGateway'
                    or source_object.__class__.__name__ == 'InclusiveGateway')
                    and source_object.gateway_direction ==
                    Gateway_direction.diverging):
                if(source_object in gw_sf_dictionary):
                    gw_sf_dictionary[source_object].append(sequence_flow)
                else:
                    gw_sf_dictionary.setdefault(source_object, [])
                    gw_sf_dictionary[source_object].append(sequence_flow)

        return(gw_sf_dictionary)

    def generate_starting_activity_set(self):
        """
        Generate a set containing the starting activity/activities of a model

        This function generates a set containing the starting activity/
        activities of the Model object.

        Returns
        -------
        starting_activity_set : set of Activity objects
            Set of Activity objects containing the starting activities of the
            model.

        Notes
        -----
        The set of starting activities is obtained by determining the
        difference between two sets. The first set, called
        'activity_target_object_list', contains a list of activities which are
        the 'target_object' of a sequence flow (i.e. these activities are not
        starting activities of the model). The second set is a set including
        all activities in the model. The difference of both sets returns a set
        containing the starting activities of the model. This set is saved as
        'starting_activity_set' and is returned.
        """
        activity_target_object_list = []

        for i in range(len(self.sequence_flow_list)):
            sequence_flow = self.sequence_flow_list[i]

            if(sequence_flow.target_object.__class__.__name__ == 'Activity'):
                activity_target_object_list.append(sequence_flow.target_object)

        activity_set = set(self.activity_list)
        activity_target_object_set = set(activity_target_object_list)
        starting_activity_set = activity_set.difference(
            activity_target_object_set)

        return(starting_activity_set)

    def connect_inclusive_gateways(self, diverging_gw, converging_gw):
        """
        Update the list containing connected inclusive gateways

        Function that adds a sublist to the 'connected_incl_gw_list' list. An
        entry in this list defines a diverging inclusive gateway and its
        corresponding inclusive gateway.

        Parameters
        ----------
        diverging_gw : InclusiveGateway object
            InclusiveGateway object with a diverging gateway direction
        converging_gw : InclusiveGateway object
            InclusiveGateway object with a converging gateway direction

        Notes
        -----
        Consider the BPMN inclusive gateway definition. In a diverging
        inclusive gateway, any nuumber of its outgoing sequence flows can be
        signaled. As a consequence, the same number of incoming sequence flows
        need to be signaled in its corresponding converging inclusive gateway.
        Hence, diverging and converging inclusive gateways are connected. The
        'connected_incl_gw_list' transmits information on connected
        diverging and converging inclusive gateways. This information is
        required for the 'inclusive_gateway_generator_function' of a converging
        inclusive gateway.
        """
        self.connected_incl_gw_list.append([diverging_gw, converging_gw])
