# -*- coding: utf-8 -*-
"""
Created on Wed Feb 04 16:46:21 2015

@author: lucp8356

"""

log = open("log.txt")
entries = []
entries_w_duration = []

#put all entries of the log in a list so that we can loop trhough it easily

for entry in log:
    if entry[0].isdigit() :
        entry = entry.replace(" ", "")
        entry = entry.rstrip()
        attributes = entry.split(";")
        
        entries.append(attributes)

#compute for each event the duration

for i in xrange(len(entries)):
    
    # Select a start event in the entries list
    
    if entries[i][4] == "start":
        entity_1 =  entries[i][1]
        event_1 = entries [i][2]
        starttime = float (entries[i][0])
        resource = entries[i][3]
        
        # look for the matching end event
        
        for j in xrange (len(entries)):
            
            if j <= i:
                continue
            elif entries [j][4] == 'end':
                entity_2 =  entries[j][1]
                event_2 = entries [j][2]
                endtime = float (entries[j][0])
                
                # Compute the duration of the activty in seconds and add
                # all information in a new entries list
                
                if entity_1 == entity_2 and event_1 == event_2:
                    duration = endtime - starttime
                    entries_w_duration.append([entity_1, event_1, 
                                               str (duration), resource])


# print the new entries list in a csv file with columns:
# 'Entity','Activtity','Duration','Resource'

log_duration = open('duration_log.csv', 'w')

for entry in entries_w_duration:
    for element in entry:
        if element is not entry[-1]:
            log_duration.write(element + ",")
            
        else:
            log_duration.write(element + "\n")
            
log_duration.close()
                    
            
            