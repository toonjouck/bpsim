# -*- coding: utf-8 -*-
"""
Created on Wed Feb 04 16:10:27 2015

@author: Toon Jouck
"""
import string

log = open("log.txt")
d = dict()
act_list = []

for entry in log:

    #omit the log info, only handle events
    #put all the attributes in a list
    #put in a dictionary all the entities as keys and traces as values
    
    if entry[0].isdigit() :
        entry = entry.replace(" ", "")
        attributes = entry.split(";")
        
        d[attributes[1]] = d.get(attributes[1], [])
        
        if attributes[2] not in d[attributes[1]] : 
            d[attributes[1]].append(attributes[2])
            
            #put al different activities in a list
            
            if attributes[2] not in act_list:
                act_list.append(attributes[2])
            
        else:
            continue
        
        
#translate activities into labels
        
labels = dict()

for i, act in enumerate(act_list):
    labels[act] = string.lowercase[i]

print labels

#now we extract from this dictionary the trace for each entity and
#write this trace on a separate line in a new control-flow log
    
cf_log = open('cf_log2.csv', 'w')
        
for entity in d:
    for event in d[entity]:
        if d[entity][-1] == event:
            cf_log.write(labels[event])
        else:
            cf_log.write(labels[event] + ",")
    cf_log.write("\n")
    
cf_log.close()
        
        
        
        