# -*- coding: utf-8 -*-
"""
Created on Wed Feb 11 10:06:45 2015

@author: lucp8356
"""

log = open("log.txt")
entries = []
new_entries = []

#put all entries of the log in a list so that we can loop trhough it easily

for entry in log:
    if entry[0].isdigit() :
        entry = entry.replace(" ", "")
        entry = entry.rstrip()
        attributes = entry.split(";")
        
        entries.append(attributes)

#search for start and end time for each activity

for i in xrange(len(entries)):
    
    # Select a start event in the entries list
    
    if entries[i][4] == "start":
        entity_1 =  entries[i][1]
        event_1 = entries [i][2]
        starttime = entries[i][0]
        resource = entries[i][3]
        
        # look for the matching end event
        
        for j in xrange (len(entries)):
            
            if j <= i:
                continue
            elif entries [j][4] == 'end':
                entity_2 =  entries[j][1]
                event_2 = entries [j][2]
                endtime = entries[j][0]
                
                # Add all information in a new entries list
                
                if entity_1 == entity_2 and event_1 == event_2:
                    new_entries.append([entity_1, event_1, 
                                               starttime, endtime, resource])

#put case attributes in log

case_attributes_log = open('case_attributes.txt')
case_attributes = []

#firstly a list is built of the case_attributes

for case in case_attributes_log:
    if case[0].isdigit() :
        case = case.replace(" ", "")
        case = case.rstrip()
        attributes = case.split(";")
        
        case_attributes.append(attributes)

#secondly we match entity numbers of case_attributes and entries in 
#the 'entries' log we have so far

for entry in new_entries:
    entity_no = int (entry[0].lstrip('entity'))
    
    for case in case_attributes:
        if int (case[0]) == entity_no:
            entry.append(case[1])
            break
                    

# print the new entries list in a csv file with columns:
# 'Entity','Activtity','Starttime','Endtime','Resource'

log_disco = open('disco_log.csv', 'w')

for entry in new_entries:
    for element in entry:
        if element is not entry[-1]:
            log_disco.write(element + ",")
            
        else:
            log_disco.write(element + "\n")
            
log_disco.close()
