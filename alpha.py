from snakes.nets import *
import snakes.plugins
snakes.plugins.load('gv', 'snakes.nets', 'nets')
from nets import *


#read file

fname = raw_input("Give the name of the file: ")

try:

    fhandle = open(fname)

except:
    print "This is an incorrect file name"
    quit()
    
#help function: traces into lists of activities

def traces_to_lists(line):
    line = line.rstrip()
    activities = line.split(',')
    return activities
    
#step 1: detect different transitions

def extract_Tl (csv_file) :
    
    Tl = []
    
    for line in csv_file:
        activities = traces_to_lists(line)
        
        for activity in activities:
            if activity in Tl:
                continue
            else:
                Tl.append(activity)
    return Tl
    
#step 2: detect start transitions

def extract_Ti (csv_file) :
    
    Ti = []
    
    for line in csv_file:
        activities = traces_to_lists(line)
        
        if activities[0] in Ti:
            continue
        else:
            Ti.append(activities[0])
    return Ti
    
#step 3: detect end transitions

def extract_To (csv_file) :
    
    To = []
    
    for line in csv_file:
        activities = traces_to_lists(line)
        
        if activities[-1] in To:
            continue
        else:
            To.append(activities[-1])
    return To
    
#step 4: detect log-based ordering relations

#4.1 detect direct succession relations

def direct_succession (csv_file):
    
    dir_s = []
        
    for line in csv_file:
        activities = traces_to_lists(line)
        
        i = 0
        
        while (i+1) < len(activities):
            t = (activities[i], activities[i+1])
            if t in dir_s:
                i = i + 1
                continue
            else:
                dir_s.append(t)
                i = i + 1
            
    return dir_s   

#4.2 detect parallel behavior

def parallel (direct_succession_list):
    
    par = []
    
    for (a, b) in direct_succession_list:
        if (b, a) in direct_succession_list and (a,b) not in par:
            if a == b:
                par.append((a,b))
            else:
                par.append((a,b))
                par.append((b,a))
        else:
            continue
            
    return par
    
#4.3 detect causal behavior

def causal_relations (direct_succession_list, parallel_list):
    
    causal = []
    
    for (a, b) in direct_succession_list:
        if (a, b) in parallel_list or (a, b) in causal:
            continue
        else:
            causal.append((a, b))
            
    return causal
    
#4.4 deduce #L relations

def deduce_other (Tl, causal_list, par_list):

    other = []
    causal_and_par = causal_list + par_list
    
    for a in Tl:
        for b in Tl:
            t = (a, b)
            t_inverse = (b, a)
            if t in causal_and_par or t_inverse in causal_and_par or t in other:
                continue
            else:
                other.append(t)
    
    return other
    
#4.5 determine Xl set

def create_candidates(k_minus_one, other_list):
    
    k = []
    
    #common antecedents
    
    for i in xrange(len(k_minus_one)):
        t1 = k_minus_one[i]
        ante1 = t1[0]
        conse1 = t1[1]
        
        for j in xrange(len(k_minus_one)):
            if j <= i:
                continue
            else:
                t2 = k_minus_one[j]
                ante2 = t2[0]
                conse2 = t2[1]
                
                if ante1 == ante2:
                    if len(conse1) > 1:
                        if conse1[0:-1] == conse2[0:-1] and conse1[-1] != conse2[-1] and (ante1, conse1 + conse2[-1]) not in k:
                            k.append((ante1, conse1 + conse2[-1]))
                    elif (ante1, conse1 + conse2) not in k or (ante1, conse2 + conse1) not in k:
                        if conse1 < conse2:
                            k.append((ante1, conse1 + conse2))
                        else:
                            k.append((ante1, conse2 + conse1))
                else:    
                    continue
                    
    #common consequents    
    
    for i in xrange(len(k_minus_one)):
        t1 = k_minus_one[i]
        ante1 = t1[0]
        conse1 = t1[1]
        
        for j in xrange(len(k_minus_one)):
            if j <= i:
                continue
            else:
                t2 = k_minus_one[j]
                ante2 = t2[0]
                conse2 = t2[1]
                
                if conse1 == conse2:
                    if len(ante1) > 1:
                        if ante1[0:-1] == ante2[0:-1] and ante1[-1] != ante2[-1] and (ante1 + ante2[-1], conse1) not in k:
                            k.append((ante1 + ante2[-1], conse1))
                    elif (ante1 + ante2[-1], conse1) not in k:
                        if ante1 < ante2:
                            k.append((ante1 + ante2, conse1))
                        else:
                            k.append((ante2 + ante1, conse1))
                else:    
                    continue
    
    return k
    
def prune(tuple, other_list, causal_list):
    
    #set_a and set_b are a list of the characters of the first and second element
    #of a tuple. Thus tuple('Start', 'End') appears as set_a=['S','t','a','r','t']
    #set_b=['E','n','d']
    
    set_a = list(tuple[0])
    set_b = list(tuple[1])
    prune = False
    
    #check between elements in set_a (=A)
    for a in set_a:
        for b in set_a:
            if (a, b) not in other_list:
                prune = True
                break
                
    #check between elements in set_b (=B)            
    for a in set_b:
        for b in set_b:
            if (a, b) not in other_list:
                prune = True
                break
                
    #check between elements in set_a and set_b  
    for a in set_a:
        for b in set_b:
            if (a, b) not in causal_list:
                prune = True
                break
           
    return prune
    
def deduce_Xl (Tl, causal_list, other_list, par_list):
    
    xl = []
    candidates = []
    k_minus_one = []
    i = 2
    
    while (i <= len(Tl)) :
        if i == 2:
            for (a, b) in causal_list:
                if not prune((a, b), other_list, causal_list):
                    xl.append((a, b))
            k_minus_one = xl
            
        else:
            candidates = create_candidates(k_minus_one, other_list)
            k_minus_one = []
            
            for (a, b) in candidates:
                if not prune((a, b), other_list, causal_list):
                    xl.append((a, b))
                    k_minus_one.append((a, b))
        i = i + 1        
    return xl
    
    
#step 5: determine Yl (=maximum decision points)

def deduce_Yl(xl):
       
    pruned = []
    yl = xl    
       
    for (a, b) in xl:
        A = set(a)
        B = set(b)
        
        for (c, d) in xl:
            if (a, b) == (c, d):
                continue
            else:
                A_prime = set(c)
                B_prime = set(d)
            
                if A <= A_prime and B <= B_prime:
                    pruned.append((a,b))
                    break
                    
    for (a, b) in pruned:
        if (a, b) in yl:
            yl.remove((a,b))
    
    return yl
    

#step 6: determine places Pl

def places(Yl):
        
    Pl = ["I", "O"]
    i = 1
    
    for (a, b) in Yl:
        Pl.append("p(" + a + "," + b + ")")
        i = i + 1
    return Pl
    
    
#step 7: specify flow relations in N

def add_arcs(Yl, Ti, To):

    Fl = []
    
    #add arcs from first/ to last places
    
    for act in Ti:
        Fl.append(("I", act))
        
    for act in To:
        Fl.append((act, "O"))
        
    #add arcs between other places and activities
    
    for (a, b) in Yl:
        set_a = list(a)
        for act in set_a:
            Fl.append((act,"p(" + a + "," + b + ")"))
            
        set_b = list(b)
        for act in set_b:
            Fl.append(("p(" + a + "," + b + ")", act))
    return Fl

#execute for specific traces

Tl = extract_Tl(fhandle)
print "Tl =", Tl

fhandle = open(fname)
Ti = extract_Ti(fhandle)
print "Ti =", Ti

fhandle = open(fname)
To = extract_To(fhandle)
print "To =", To

fhandle = open(fname)
dir_s = direct_succession(fhandle)
print ">L =", dir_s

par = parallel(dir_s)
print "||L =", par

causal = causal_relations(dir_s, par)
print "->L =", causal

other = deduce_other(Tl, causal, par)
print "#L =", other

Xl = deduce_Xl(Tl, causal, other, par)
print "Xl =", Xl

Yl = deduce_Yl(Xl)
print "Yl =", Yl

Pl = places(Yl)
print "Pl =", Pl

Fl = add_arcs(Yl, Ti, To)
print "Fl =", Fl

#Put everything in a PetriNet object to output a pnml file

n = PetriNet('N')

for place in Pl:
    n.add_place(Place(place))
    
for transition in Tl:
    n.add_transition(Transition(transition))
    
for (a, b) in Fl:
    if a.startswith('p(') or a == 'I' or a == 'O':
        n.add_input(a, b, Value(dot))
    else:
        n.add_output(b, a, Value(dot))
        
discovered_model = open('alpha_model.pnml', 'w')

discovered_model.write(dumps(n))

discovered_model.close()
    
    
    
    
    
    
    